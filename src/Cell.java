import javax.swing.*;
import java.awt.*;

public class Cell extends JFrame
{
    private int cellHeight = 0, cellWidth = 0, cellX = 0, cellY = 0;
    public boolean isMousePosition = false;

    public void paint (Graphics g, int x, int y )
    {
        double mouseX = getMousePosition().getX();
        double mouseY = getMousePosition().getY();
        isMousePosition(mouseX, mouseY);
        if(isMousePosition == false)    
            g.setColor(Color.WHITE);
        else if (isMousePosition == true)
            g.setColor(Color.GRAY);
        g.fillRect(x, y, cellWidth, cellHeight);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, cellWidth, cellHeight);        
    }

    public void run()
    {
        while(true)
        {
            this.repaint();
        }
    }

    public void isMousePosition (double x, double y)
    {
        Boolean isInX = false, isInY = false;
        isMousePosition = false;

        if(x > cellX && x < cellX + cellWidth)
            isInX = true;
        if(y < cellY && y > cellY + cellHeight)
            isInY = true;

        if(isInY && isInX)
            isMousePosition = true;
    }

    Cell ()
    {
        cellHeight = 35;
        cellWidth = 35;
    }

    public int getHeight()
    {
        return cellHeight;
    }

    public int getWidth()
    {
        return cellWidth;
    }

    public int getX()
    {
        return cellX;
    }

    public int getY()
    {
        return cellY;
    }
    public void setX(int x)
    {
        cellX = x;
    }

    public void setY(int y)
    {
        cellY = y;
    }
}
import javax.swing.*;
import java.awt.*;

public class Grid extends JFrame
{  
    public Cell cell = new Cell();
    public Cell cells[];
    private int gridWidth = 0, gridHeight = 0, windowWidth =0;
    private int windowHeight = 0, border = 0;

    public void paint (Graphics g)
    {
        g.setColor(Color.BLACK);
        for(int i = border; i < windowHeight - border; i+= cell.getHeight())
            {
                for(int j= border; j < windowWidth - border; j += cell.getWidth())
                {
                    cell.paint(g,i,j);
                    cell.setY(j);
                    cell.setX(i);
                }
            }
    }
    
    public void run()
    {
        
    }

    public int getGridHeight()
    {
        return gridHeight;
    }

    public int getGridWidth()
    {
        return gridWidth;
    }

    Grid()
    {
        int cellNumber = 0;
        gridWidth = 20;
        gridHeight = 20;
        windowHeight = 720;
        windowWidth = 720;
        border = 10;

        for(int i = border; i < windowHeight - border; i+= cell.getHeight())
        {
            for(int j= border; j < windowWidth - border; j += cell.getWidth())
            {
                    cells[cellNumber].setY(j);
                    cells[cellNumber].setX(i);
                    cellNumber++;
            }
        }
    }

    public void isMousePosition (double x, double y, int cellNumber)
    {
        Boolean isInX = false, isInY = false;
        cells[cellNumber].isMousePosition = false;

        if(x > cells[cellNumber].getX() 
        && x < cells[cellNumber].getX() + cells[cellNumber].getWidth())
            isInX = true;
        if(y < cells[cellNumber].getY()
        && y > cells[cellNumber].getY() + cells[cellNumber].getHeight())
            isInY = true;

        if(isInY && isInX)
        cells[cellNumber].isMousePosition = true;
    }

    public void setGridDimensions(int width, int height)
    {
        gridWidth = width;
        gridHeight = height;
    }

    
}